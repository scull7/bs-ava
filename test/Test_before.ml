open Ava

let before_promise_all_counter = ref 0;;

let () =
  Before.promiseAll ava (fun _ ->
    let _ = Js.Console.log "\n\nHello!\n\n" in
    let v = !before_promise_all_counter
    in
    (before_promise_all_counter := v + 1 ; ())
    |> Js.Promise.resolve
  );

  After.all ava (fun t -> Assert.is t 1 !before_promise_all_counter);

  test "before and after test functions" (fun t -> Test.pass t);

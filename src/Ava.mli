type ava

val ava : ava

module Test : sig
  type 'a t

  val contextGet : 'a t -> 'a

  val contextSet : 'a t -> 'a -> unit

  val plan : 'a t -> int -> unit

  val pass : ?message:string -> 'a t -> unit

  val fail : ?message:string -> 'a t -> unit

  val finish : ?message:string -> 'a t -> unit
end

module Assert : sig
  type 'a t = 'a Test.t

  val truthy : ?message:string -> 'a t -> 'b -> unit

  val falsy : ?message:string -> 'a t -> 'b -> unit

  val true_ : ?message:string -> 'a t -> 'b -> unit

  val false_ : ?message:string -> 'a t -> 'b -> unit

  val is : ?message:string -> 'a t -> 'b -> 'b -> unit

  val isnt : ?message:string -> 'a t -> 'b -> 'b -> unit

  val deepEqual : ?message:string -> 'a t -> 'b -> 'b -> unit

  val notDeepEqual : ?message:string -> 'a t -> 'b -> 'b -> unit

  val throws :
    ?message:string ->
    expected:'a ->
    'b t ->
    (unit -> unit) ->
    unit

  val notThrows :
    ?message:string ->
    'a t ->
    (unit -> unit) ->
    unit

  val regex : ?message:string -> 'a t -> Js.Re.t -> string -> unit

  val notRegex : ?message:string -> 'a t -> Js.Re.t -> string -> unit
end

val test : string -> ('a Test.t -> unit) -> unit

val testPromise : string -> ('a Test.t -> 'b Js.Promise.t) -> unit

module After : sig
  val all : ava -> ?message:string -> ('a Test.t -> unit) -> unit

  val promiseAll :
    ava -> ?message:string -> ('a Test.t -> 'b Js.Promise.t) -> unit

  val each : ava -> ?message:string -> ('a Test.t -> unit) -> unit

  val promiseEach :
    ava -> ?message:string -> ('a Test.t -> 'b Js.Promise.t) -> unit
end

module Async : sig
  val test : ava -> string -> ('a Test.t -> unit) -> unit
end

module Before : sig
  val all : ava -> ?message:string -> ('a Test.t -> unit) -> unit

  val promiseAll :
    ava -> ?message:string -> ('a Test.t -> 'b Js.Promise.t) -> unit

  val each : ava -> ?message:string -> ('a Test.t -> unit) -> unit

  val promiseEach :
    ava -> ?message:string -> ('a Test.t -> 'b Js.Promise.t) -> unit
end

module Failing : sig
  val test : ava -> string -> ('a Test.t -> unit) -> unit

  val testPromise : ava -> string -> ('a Test.t -> 'b Js.Promise.t) -> unit
end

module Only : sig
  val test : ava -> string -> ('a Test.t -> unit) -> unit

  val testPromise : ava -> string -> ('a Test.t -> 'b Js.Promise.t) -> unit
end

module Serial : sig
  val test : ava -> string -> ('a Test.t -> unit) -> unit

  val testPromise : ava -> string -> ('a Test.t -> 'b Js.Promise.t) -> unit
end

module Skip : sig
  val test : ava -> string -> ('a Test.t -> unit) -> unit

  val testPromise : ava -> string -> ('a Test.t -> 'b Js.Promise.t) -> unit
end

module Todo : sig
  val test : ava -> string -> unit
end
